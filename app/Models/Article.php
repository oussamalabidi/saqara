<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $fillable = [
        'name',
        'reference',
        'unit_price'
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_articles');
    }
}
