<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use CrudTrait;
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'order_articles');
    }

    public function getArticlesCountAttribute()
    {
        return $this->articles()->count();
    }
}
